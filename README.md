# [Adyax test task v1.0.0](https://bitbucket.org/selcet/adyax-test-task/src)
### Adyax - test task

## Environment Tools
- NodeJS (for downloading visit page - https://nodejs.org/en/download/)
- Gulp and Gulp modules. *Just run file*:
 - `./gulpCompile.sh` *- for* **MacOS** *or* **Linux** *system and*
 - `gulpCompile.bat` *for* **WindowsOS**

## Running page
All you need - run file
- `./gulpWatch.sh` *- for* **MacOS** *or* **Linux** *system and*
- `gulpWatch.bat` *for* **WindowsOS**

## Author
- Email: mr.selcet@gmail.com
- Skype: selcet (Vladimir Kostenko)
- Linkedin: https://www.linkedin.com/in/selcet
- GitHub: https://github.com/selcet
