/**
 * Created by Valdemar Kostenko on 3/21/16 at 10:33 PM.
 */

// =====================================
// Realisation with jQuery library
// =====================================
$(document).ready(function() {
  $(".tabs-menu a").click(function(event) {
    event.preventDefault();
    $(this).parent().addClass("current");
    $(this).parent().siblings().removeClass("current");
    var tab = $(this).attr("href");
    $(".tab-items-content").not(tab).css("display", "none");
    $(tab).fadeIn();
  });
});
// =====================================

// =====================================
// Alternative way for Tabs with pure JS
// =====================================
//// tab-items
//var links = document.getElementsByClassName("tab-items-title");
//// grab p tags
//var paragraphs = document.getElementsByClassName("tab-items-content");
//
//// bind event handlers in most simple way
//for(var i = 0; i < links.length; i++) {
//  links[i].onclick = function() {
//    hideAllParagraphs();
//    var link = this;
//    href = link.href;
//    href = href.slice(href.indexOf("#")+1,href.length);
//    document.getElementById(href).style.display = "block";
//  }
//}
//
//function hideAllParagraphs(ignoreFirst) {
//  for(var i = 0; i < paragraphs.length; i++) {
//    if(ignoreFirst && i == 0) continue;
//    paragraphs[i].style.display = "none";
//  }
//}
//
//hideAllParagraphs(true);
// =====================================